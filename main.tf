# Define the security group for the cluster
resource "aws_security_group" "ecs_sg" {
    vpc_id      = var.vpc_id

    ingress {
        from_port       = 443
        to_port         = 443
        protocol        = "tcp"
        cidr_blocks     = ["0.0.0.0/0"]
    }

    egress {
        from_port       = 0
        to_port         = 65535
        protocol        = "tcp"
        cidr_blocks     = ["0.0.0.0/0"]
    }
}

#Setup IAM policy for the cluster and autoscaling group
#Role Definition (Ec2 Access to start with)
data "aws_iam_policy_document" "ecs_perms" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "ecs_perms" {
  name               = "ecs_perms"
  assume_role_policy = data.aws_iam_policy_document.ecs_perms.json
}


resource "aws_iam_role_policy_attachment" "ecs_perms" {
  role       = aws_iam_role.ecs_perms.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}
#Create the instance profile and attach the previously defined role
resource "aws_iam_instance_profile" "ecs_perms" {
  name = "ecs-perms"
  role = aws_iam_role.ecs_perms.name
}

# Create the autoscaling group, and configuration 
# To create an autoscaling group you must first define a launch config so it knows 
# what type of instance to create. 
resource "aws_launch_configuration" "ecs_launch_config" {
    image_id             = var.AMIID
    #Grab our previously defined role for the instance
    iam_instance_profile = aws_iam_instance_profile.ecs_perms.name
    security_groups      = [aws_security_group.ecs_sg.id]
    #Generically I will define a basic user_data profile for this test
    user_data            = "#!/bin/bash\necho ECS_CLUSTER=my-cluster >> /etc/ecs/ecs.config"
    #I will har code this one, as I do not intened to leave the free tier for this project. I would typically 
    #make this a variable and pass that in there ansible extra vars from AWX
    instance_type        = "t2.micro"
}

resource "aws_autoscaling_group" "lendflow_asg" {
    name                      = "lendflow-asg"
    vpc_zone_identifier       = ["subnet-0fe83f1037b48540c"]
    launch_configuration      = aws_launch_configuration.ecs_launch_config.name

    #For now, I will just create the cluster as a one server. 
    #This will allow the site to be fault tollerant, but not highly available. 
    #You can have this set to deired capacity of 2 and you will always have two servers
    #To route traffice to. 
    desired_capacity          = 1
    min_size                  = 1
    max_size                  = 1
    health_check_grace_period = 300
    health_check_type         = "EC2"
}

#Define the ECS Cluster
resource "aws_ecs_cluster" "ecs_cluster" {
    name  = "lendflow-cluster"
}

#Load our docker definition in from the docker.json file. 
data "template_file" "task_definition_template" {
    template = file("docker.json")
    #You can templetize this for more configuration, my time is limited here though
}
#Create task-definition for the ECS cluster to know what to run for the containers from the file above
resource "aws_ecs_task_definition" "task_definition" {
  family                = "node"
  container_definitions = data.template_file.task_definition_template.rendered
}

#Creating the actual ecs-service now
resource "aws_ecs_service" "node" {
  name            = "node"
  cluster         = aws_ecs_cluster.ecs_cluster.id
  task_definition = aws_ecs_task_definition.task_definition.arn
  #Once again running just one for the demo
  desired_count   = 1
}



