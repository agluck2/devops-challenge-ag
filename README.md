
## LendFlow ECS Cluster Deployment
---
FEEDBACK:
I do not mean ANY disrespect by anything below, your README mentioned giving feedback. 

I do not mind working on a challenge to get into a job interview, but for feedback sake:
From experience, this challenge seems too inclusive; if you are not targeting a kid fresh out of school, who does not have a family and a full time job.

If the challenge was create a terraform script that would build out an ECS cluster, and everything needed with it, that would have made a ton more sense, in my opinion (and was all I could give you). 
Making a person create all these accounts, build out a bunch of stuff, and then give you access to that account, is a little much. I understand if I do not get an interview, and I am completely okay with that. I really do hope that you find a great engineer to fill this role. 

I have been doing this for 9 years now and am the senior devops engineer at my company, we would never have a challenge this involved and expect to get a list of good candidates. We hand them a laptop and give them a little bit of time in the beginning of the interview to create a simple script, or have them talk through the process. 

I hope this is valuable feedback, and again, I do not mean to be disrespectful if it comes across that way. My opinions here could be completely misplaced, and this may prove very effective.

---
My time is very limited, so I will have to be very concise with this, and I will need to make a lot of assumptions. 

### Assumptions:
- You have a working knowledge of git
- You have working knowledge of basic linux
- You currently have terraform installed on your local machine (I typically drop mine in /usr/local/bin, for reference). 
- You have already created a VPC and 1 or more AZs within that VPC
- Terraform has AWS programatic access to the account. (This can be set one of three ways, please see the AWS documentation for setting programatic access)
- You have given the terraform user appropriate permissions to execute in the AWS environment. 

### How To build default cluster: 
1. Pull this project down to your local environment where you have terraform installed
2. Run terraform init
3. Run terraform plan
4. Review plan, if all looks good run terraform apply 

I have verified that the plan succeeds. This terraform should work and get you 90% there. I did what I could in 30 minutes after work today. 
I have included the output of the plan here in the repo, as well as all of the state information. 

TODO:
Still need to push static code to docker image