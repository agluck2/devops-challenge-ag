variable "AMIID" {
  type = string
  #Default Amazon2 instance
  default = "ami-0c2b8ca1dad447f8a"
}

variable "vpc_id" {
    type = string
    default = "vpc-07f29fa07ded6ba00"
}

variable "subnetId" {
  type = string
  default = "subnet-0fe83f1037b48540c"
}